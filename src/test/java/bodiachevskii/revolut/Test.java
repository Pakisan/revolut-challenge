package bodiachevskii.revolut;

import bodiachevskii.revolut.transaction.model.Transaction;
import bodiachevskii.revolut.transaction.repository.TransactionRepository;
import lombok.extern.slf4j.Slf4j;

import java.math.BigInteger;
import java.util.List;



@Slf4j
public class Test {

    @org.junit.Test
    public void test() throws Exception {
        TransactionRepository transactionRepository = new TransactionRepository();

        List<Transaction> transactions = transactionRepository.get();

        log.info("Found transactions:\n{}", transactions);

        Transaction transaction = transactionRepository.save(new Transaction(null,
                BigInteger.valueOf(18288747L),
                BigInteger.valueOf(17371637L),
                BigInteger.valueOf(99999999L))
        );

        Transaction transaction2 = transactionRepository.save(new Transaction(null,
                BigInteger.valueOf(18_288_747L),
                BigInteger.valueOf(17_371_637L),
                BigInteger.valueOf(99_999_999L))
        );

        log.info("Saved transactions:\n{}", transaction);
        log.info("Saved transactions:\n{}", transaction2);

        log.info("Transaction with id=1:\n{}", transactionRepository.get(BigInteger.valueOf(1L)));
        log.info("Transaction with id=2:\n{}", transactionRepository.get(BigInteger.valueOf(2L)));

        log.info("Found transactions:\n{}", transactionRepository.get());
    }

}
