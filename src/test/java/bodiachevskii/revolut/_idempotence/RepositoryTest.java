package bodiachevskii.revolut._idempotence;

import bodiachevskii.revolut._idempotence.model.Idempotence;
import bodiachevskii.revolut._idempotence.repository.IdempotenceRepository;
import bodiachevskii.revolut.transaction.model.dto.TransactionDTO;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Assert;
import org.junit.Test;

import javax.persistence.PersistenceException;
import java.math.BigInteger;

public class RepositoryTest {

    private IdempotenceRepository idempotenceRepository = new IdempotenceRepository();

    @Test
    public void save() throws Exception {
        TransactionDTO transactionDTO = new TransactionDTO(
                BigInteger.valueOf(39_874_231L), BigInteger.valueOf(39_874_231L), BigInteger.valueOf(39_874_231L)
        );

        Idempotence idempotence = new Idempotence(null, "8d8sf00fs-fsfsff-djdj8877ad", "/api/v1/transactions", String.valueOf(transactionDTO.hashCode()));

        idempotenceRepository.save(idempotence);
    }

    @Test
    public void saveExisted() throws Exception {
        TransactionDTO transactionDTO = new TransactionDTO(
                BigInteger.valueOf(39_874_231L), BigInteger.valueOf(39_874_231L), BigInteger.valueOf(39_874_231L)
        );

        Idempotence idempotence = new Idempotence(null, "8d8sf00fs-fsfsff-djdj8877ad", "/api/v1/transactions", String.valueOf(transactionDTO.hashCode()));

        idempotenceRepository.save(idempotence);

        try {
            idempotenceRepository.save(idempotence);
        } catch (PersistenceException persistenceException) {
            Assert.assertEquals(ConstraintViolationException.class, persistenceException.getCause().getClass());
        }
    }

}
