package bodiachevskii.revolut._idempotence;

import bodiachevskii.revolut._idempotence.exception.IdempotentException;
import bodiachevskii.revolut._idempotence.service.IdempotenceService;
import bodiachevskii.revolut.transaction.model.dto.TransactionDTO;
import org.junit.Test;

public class ServiceTest {

    private IdempotenceService idempotenceService = new IdempotenceService();

    @Test
    public void registerRequest() throws Exception {
        TransactionDTO transactionDTO = new TransactionDTO();

        idempotenceService.registerRequest("293984-djjahdhad-23393", "/api/v1/transactions", transactionDTO);
    }

    @Test(expected = IdempotentException.class)
    public void reRegisterRequest() throws Exception {
        TransactionDTO transactionDTO = new TransactionDTO();

        idempotenceService.registerRequest("293984-djjahdhad-23393", "/api/v1/transactions", transactionDTO);
        idempotenceService.registerRequest("293984-djjahdhad-23393", "/api/v1/transactions", transactionDTO);
    }

}
