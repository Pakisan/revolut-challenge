package bodiachevskii.revolut.transaction;

import bodiachevskii.revolut.transaction.model.Transaction;
import bodiachevskii.revolut.transaction.repository.TransactionRepository;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.LinkedList;

public class RepositoryTest {

    private TransactionRepository transactionRepository = new TransactionRepository();

    @Test(expected = NullPointerException.class)
    public void singleCreationWithoutValues() {
        new Transaction(
                null,
                null,
                null,
                null
        );
    }

    @Test
    public void singleCreationWithoutId() throws Exception {
        Transaction transaction = new Transaction(
                null,
                BigInteger.valueOf(56_000_000L),
                BigInteger.valueOf(46_000_000L),
                BigInteger.valueOf(99_999_999L)
        );

        transactionRepository.save(transaction);

        Assert.assertEquals(transaction, transactionRepository.get(transaction.getId()));
    }

    @Test
    public void singleCreationWithId() throws Exception {
        Transaction transaction = new Transaction(
                BigInteger.valueOf(1_000_000_000L),
                BigInteger.valueOf(56_000_000L),
                BigInteger.valueOf(46_000_000L),
                BigInteger.valueOf(99_999_999L)
        );

        transactionRepository.save(transaction);

        Assert.assertEquals(transaction, transactionRepository.get(transaction.getId()));
    }

    @Test
    public void getOne() throws Exception {
        Transaction transaction_1 = new Transaction(null, BigInteger.valueOf(56_000_000L), BigInteger.valueOf(46_000_000L), BigInteger.valueOf(99_999_999L));
        Transaction transaction_2 = new Transaction(null, BigInteger.valueOf(59_000_000L), BigInteger.valueOf(76_000_000L), BigInteger.valueOf(99_000L));

        transactionRepository.save(transaction_1);
        transactionRepository.save(transaction_2);

        Assert.assertNull(transactionRepository.get(BigInteger.valueOf(-1L)));

        Assert.assertEquals(transaction_2, transactionRepository.get(transaction_2.getId()));
    }

    @Test
    public void getSeveral() throws Exception {
        Transaction transaction_1 = new Transaction(null, BigInteger.valueOf(56_000_000L), BigInteger.valueOf(46_000_000L), BigInteger.valueOf(99_999_999L));
        Transaction transaction_2 = new Transaction(null, BigInteger.valueOf(59_000_000L), BigInteger.valueOf(76_000_000L), BigInteger.valueOf(99_000L));

        transactionRepository.save(transaction_1);
        transactionRepository.save(transaction_2);

        Assert.assertEquals(Arrays.asList(transaction_1, transaction_2), transactionRepository.get());
    }

}
