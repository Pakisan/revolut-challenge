package bodiachevskii.revolut._http;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiResponse<DataType> {

    private int status;
    private String message;
    private DataType data;

}
