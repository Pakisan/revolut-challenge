package bodiachevskii.revolut._http;

import com.google.gson.Gson;
import spark.Response;

public class ResponseUtil {

    private static Gson gson = new Gson();

    public static <ResponseDataType> String ok(ResponseDataType responseData, Response response) {
        ApiResponse<ResponseDataType> apiResponse = new ApiResponse<>(200, "ok.", responseData);

        response.status(200);
        response.type("application/json");

        return gson.toJson(apiResponse);
    }

    public static <ResponseDataType> String created(ResponseDataType responseData, Response response) {
        ApiResponse<ResponseDataType> apiResponse = new ApiResponse<>(201, "ok.", responseData);

        response.status(201);
        response.type("application/json");

        return gson.toJson(apiResponse);
    }

    public static <ResponseDataType> String notFound(ResponseDataType responseData, Response response) {
        ApiResponse<ResponseDataType> apiResponse = new ApiResponse<>(404, "not found.", responseData);

        response.status(404);
        response.type("application/json");

        return gson.toJson(apiResponse);
    }

    public static <ResponseDataType> String bad(ResponseDataType responseData, Response response, String message) {
        ApiResponse<ResponseDataType> apiResponse = new ApiResponse<>(400, message, responseData);

        response.status(400);
        response.type("application/json");

        return gson.toJson(apiResponse);
    }

}
