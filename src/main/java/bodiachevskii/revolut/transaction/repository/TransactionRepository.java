package bodiachevskii.revolut.transaction.repository;

import bodiachevskii.revolut._hibernate.HibernateSessionFactoryUtil;
import bodiachevskii.revolut.transaction.model.Transaction;
import org.hibernate.Session;

import java.math.BigInteger;
import java.util.List;

public class TransactionRepository {

    /**
     * Saves given {@link Transaction}.
     * @param transaction {@link Transaction} to save.
     * @return saved {@link Transaction}.
     *
     * @throws Exception in case of database interactions error.
     */
    public Transaction save(Transaction transaction) throws Exception {
        Session session = HibernateSessionFactoryUtil.sessionFactory.openSession();
        org.hibernate.Transaction hTransaction = session.getTransaction();

        hTransaction.begin();

        try {
            session.save(transaction);
            hTransaction.commit();

            return transaction;
        } catch (Exception e) {
            hTransaction.rollback();
            throw new Exception("");
        }
    }

    /**
     * Tries to get {@link Transaction} with provided ID.
     * @param id Transaction's ID to try to find.
     * @return {@link Transaction} or NULL.
     */
    public Transaction get(BigInteger id) {
        return HibernateSessionFactoryUtil.sessionFactory.openSession().get(Transaction.class, id);
    }

    /**
     * Tries to get {@link Transaction}s.
     * @return {@link List} with {@link Transaction} or empty.
     */
    public List<Transaction> get() {
       return (List<Transaction>)  HibernateSessionFactoryUtil.sessionFactory.openSession().createQuery("From Transaction").list();
    }

}
