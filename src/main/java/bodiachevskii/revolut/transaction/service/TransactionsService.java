package bodiachevskii.revolut.transaction.service;

import bodiachevskii.revolut.transaction.model.Transaction;
import bodiachevskii.revolut.transaction.model.dto.TransactionDTO;
import bodiachevskii.revolut.transaction.repository.TransactionRepository;

public class TransactionsService {

    private final TransactionRepository transactionRepository = new TransactionRepository();

    public void handle(TransactionDTO transactionDTO) throws Exception {
        transactionRepository.save(
                new Transaction(null, transactionDTO.getIssuerId(), transactionDTO.getReceiverId(), transactionDTO.getValue())
        );
    }

}
