package bodiachevskii.revolut.transaction.controller;

import bodiachevskii.revolut._http.ResponseUtil;
import bodiachevskii.revolut._idempotence.exception.IdempotentException;
import bodiachevskii.revolut._idempotence.service.IdempotenceService;
import bodiachevskii.revolut.transaction.model.Transaction;
import bodiachevskii.revolut.transaction.model.dto.TransactionDTO;
import bodiachevskii.revolut.transaction.repository.TransactionRepository;
import bodiachevskii.revolut.transaction.service.TransactionsService;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import spark.Request;
import spark.Response;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class TransactionController {

    private static Gson gson = new Gson();
    private static TransactionsService transactionsService = new TransactionsService();
    private static TransactionRepository transactionRepository = new TransactionRepository();
    private static IdempotenceService idempotenceService = new IdempotenceService();

    public static String all(Request request, Response response) {
        List<Transaction> transactions = transactionRepository.get();

        if (transactions.isEmpty()) {
            return ResponseUtil.notFound(null, response);
        } else {
            List<TransactionDTO> dtos = new LinkedList<>();

            transactions.forEach(transaction -> {
                dtos.add(new TransactionDTO(transaction.getIssuerId(), transaction.getReceiverId(), transaction.getValue()));
            });

            return ResponseUtil.ok(dtos, response);
        }
    }

    public static String byId(Request request, Response response) {
        BigInteger id;

        try {
            id = BigInteger.valueOf(Long.parseLong(request.params(":transactionId")));
        } catch (Exception e) {
            return ResponseUtil.bad(null, response, "corrupted id.");
        }

        Transaction transaction = transactionRepository.get(id);

        if (Objects.isNull(transaction)) {
            return ResponseUtil.notFound(null, response);
        } else {
            TransactionDTO transactionDTO = new TransactionDTO(transaction.getIssuerId(), transaction.getReceiverId(), transaction.getValue());

            return ResponseUtil.ok(transactionDTO, response);
        }
    }

    public static String create(Request request, Response response) {
        TransactionDTO transaction;

        if (Objects.isNull(request.headers("request-key"))) {
            return ResponseUtil.bad(null, response, "missing request-key.");
        }

        if (Objects.isNull(request.headers("idempotence-key"))) {
            return ResponseUtil.bad(null, response, "missing idempotence-key.");
        }

        try {
            transaction = gson.fromJson(request.body(), TransactionDTO.class);

            idempotenceService.registerRequest(request.headers("idempotence-key"), request.servletPath(), transaction);
            transactionsService.handle(transaction);

            return ResponseUtil.created(null, response);
        } catch (JsonSyntaxException jsonSyntaxException) {
            return ResponseUtil.bad(null, response, "corrupted payload.");
        } catch (IdempotentException idempotentException) {
            return ResponseUtil.bad(null, response, "request already accepted.");
        } catch (Exception exception) {
            return ResponseUtil.bad(null, response, "server side error.");
        }
    }

}
