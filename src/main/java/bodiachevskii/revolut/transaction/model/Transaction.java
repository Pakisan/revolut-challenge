package bodiachevskii.revolut.transaction.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigInteger;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "transactions")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transactions_sequence")
    @SequenceGenerator(name = "transactions_sequence", sequenceName = "transactions_sequence", initialValue = 1, allocationSize = 1)
    private BigInteger id;

    @NonNull
    @Column(nullable = false, name = "issuer_id")
    private BigInteger issuerId;

    @NonNull
    @Column(nullable = false, name = "receiver_id")
    private BigInteger receiverId;

    @NonNull
    @Column(nullable = false, name = "value")
    private BigInteger value;

}
