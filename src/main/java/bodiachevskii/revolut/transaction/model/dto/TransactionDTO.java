package bodiachevskii.revolut.transaction.model.dto;

import lombok.*;

import java.math.BigInteger;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class TransactionDTO {

    @NonNull
    private BigInteger issuerId;
    @NonNull
    private BigInteger receiverId;
    @NonNull
    private BigInteger value;

}
