package bodiachevskii.revolut;

import bodiachevskii.revolut.transaction.controller.TransactionController;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;

import static spark.Spark.*;

@Slf4j
public class Application {

    private static Gson gson = new Gson();

    /*
        content-type: application-json;encoding=utf-8
     */
    public static void main(String[] args) {
        port(8080);

        path("/api/v1/transactions", () -> {
            get("", "application/json", TransactionController::all);
            get("/:transactionId", "application/json", TransactionController::byId);
            post("", "application/json", TransactionController::create);
        });

    }

}
