package bodiachevskii.revolut._idempotence.exception;

public class IdempotentException extends RuntimeException {

    public IdempotentException(String message) {
        super(message, null);
    }

    public IdempotentException(String message, Throwable cause) {
        super(message, cause);
    }

}
