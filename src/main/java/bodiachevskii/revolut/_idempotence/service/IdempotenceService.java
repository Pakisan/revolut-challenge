package bodiachevskii.revolut._idempotence.service;

import bodiachevskii.revolut._idempotence.exception.IdempotentException;
import bodiachevskii.revolut._idempotence.model.Idempotence;
import bodiachevskii.revolut._idempotence.repository.IdempotenceRepository;
import bodiachevskii.revolut.transaction.model.dto.TransactionDTO;
import org.hibernate.exception.ConstraintViolationException;

import javax.persistence.PersistenceException;

public class IdempotenceService {

    private IdempotenceRepository idempotenceRepository = new IdempotenceRepository();

    /**
     * Tries to register request to avoid
     * @param idempotenceKey idempotence key.
     * @param path requested path.
     * @param transactionDTO received {@link TransactionDTO}.
     *
     * @throws Exception in case of un-handled error.
     * @throws IdempotentException in case of already accepted request.
     */
    public void registerRequest(String idempotenceKey, String path, TransactionDTO transactionDTO) throws Exception, IdempotentException {
        try {
            idempotenceRepository.save(new Idempotence(null, idempotenceKey, path, String.valueOf(transactionDTO.hashCode())));
        } catch (PersistenceException persistenceException) {
            if (persistenceException.getCause().getClass() == ConstraintViolationException.class) {
                throw new IdempotentException("Request already accepted.");
            }
        }
    }

}
