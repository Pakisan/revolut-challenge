package bodiachevskii.revolut._idempotence.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigInteger;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "idempotence", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"idempotence_key", "path", "payload_hash"})
})
public class Idempotence {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idempotence_sequence")
    @SequenceGenerator(name = "idempotence_sequence", sequenceName = "idempotence_sequence", initialValue = 1, allocationSize = 1)
    private BigInteger id;
    /* 8d8sf00fs-fsfsff-djdj8877ad */
    @NonNull
    @Column(nullable = false, name = "idempotence_key")
    private String idempotenceKey;
    /* /api/v1/transactions */
    @NonNull
    @Column(nullable = false, name = "path")
    private String path;
    /* jdbadyad7a77d7a dy bdybuabd */
    @NonNull
    @Column(nullable = false, name = "payload_hash")
    private String payloadHash;

}
