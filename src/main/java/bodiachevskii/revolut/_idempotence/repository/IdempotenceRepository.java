package bodiachevskii.revolut._idempotence.repository;

import bodiachevskii.revolut._hibernate.HibernateSessionFactoryUtil;
import bodiachevskii.revolut._idempotence.model.Idempotence;
import org.hibernate.Session;

public class IdempotenceRepository {

    public void save(Idempotence idempotence) throws Exception {
        Session session = HibernateSessionFactoryUtil.sessionFactory.openSession();
        org.hibernate.Transaction hTransaction = session.getTransaction();

        hTransaction.begin();

        try {
            session.save(idempotence);
            hTransaction.commit();
        } catch (Exception e) {
            hTransaction.rollback();
            throw e;
        }
    }

}
